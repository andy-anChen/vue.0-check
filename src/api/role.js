import request from '@/utils/request';

// 分页获取角色列表
export function getRoleList(data) {
    return request({
      url: '/spring/role',
      method: 'get',
      params:data
    })
  }

// 新增角色
  export function addRole(data) {
    return request({
      url: '/spring/role/add',
      method: 'post',
      data
    })
  }

  //根据id删除角色信息
export function deleteRole(id) {
  return request({
    url: `/spring/role/delete/${id}`,
    method: 'delete'
  })
}

//修改角色
export function updateRole(data) {
  return request({
    url: '/spring/role/update',
    method: 'put',
    data
  })
}

//修改角色状态
export function updateState(data) {
  return request({
    url: '/spring/role/updateState',
    method: 'put',
    params: data
  })
}