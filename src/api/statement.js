import request from "@/utils/request";

// 分页模糊查询明细报表

export function selectDetailReport(data) {
  return request({
    url: "/spring/punchingCard/selectDetailReport",
    method: "post",
    data,
  });
}

// 分页模糊查询汇总信息

export function select(data) {
  return request({
    url: "/spring/collect/select",
    method: "post",
    data,
  });
}
