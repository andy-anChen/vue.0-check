import request from '@/utils/request'

// 供应商管理 ————分页加模糊查询
export function getRequest(data) {
    return request({
      url: 'spring/provider',
      method: 'get',
      params:data
    })
  }
  
  // 添加供应商信息

export function addSupplier(data){
    return request({
      url:'/spring/provider/add',
      method:'post',
      data
    })
  }

  // 根据id删除供应商信息
  export function deleteSupplier(id){
      return request({
        url:`/spring/provider/delete/${id}`,
        method:'delete',
      })
    }

    // 修改供应商信息
    export function editSupplier(data){
        return request({
          url:'/spring/provider/update',
          method:'put',
          data
        })
      }