import request from '@/utils/request';

// 模糊查询所有的项目信息
export function getItemList(data){
    return request({
      url:'/spring/project/selectProject',
      method:'post',
      data
    })
  }

  // 根据业务线id分页模糊查询所有的项目信息
  export function selectProjectByBusinessId(data){
      return request({
        url:'/spring/project/selectProjectByBusinessId',
        method:'post',
        data
      })
    }
//   添加项目列表信息
export function addItem(data){
    return request({
      url:'/spring/project/add',
      method:'post',
      data
    })
  }

// 根据id修改项目列表信息
export function editItem(data){
    return request({
      url:'/spring/project/update',
      method:'put',
      data
    })
  }

// 根据id结束项目
export function updateClosingState(data){
    return request({
      url:'/spring/project/updateClosingState',
      method:'put',
      params:data
    })
  }

// 分页查询成员
export function selectMember(data){
    return request({
      url:'/spring/project/selectMember',
      method:'get',
      params:data
    })
  }

// 分页模糊查询未进入项目的员工
export function selectIsLeader(data){
    return request({
      url:'/spring/project/selectIsLeader',
      method:'get',
      params:data
    })
  }

// 根据id将成员移出项目
export function removed(data){
    return request({
      url:'/spring/project/removed',
      method:'put',
      params:data
    })
  }

// 添加成员信息
export function addMember(data){
    return request({
      url:'/spring/project/addMember',
      method:'post',
      data
    })
  }

// 将成员设为负责人
export function setAsLeader(data){
    return request({
      url:'/spring/project/setAsLeader',
      method:'put',
      params:data
    })
  }

// 分页模糊查询打卡信息

export function selectPunchingCard(data){
    return request({
      url:'/spring/punchingCard/selectPunchingCard',
      method:'get',
      params:data
    })
  }

// 根据id调整信息
export function adjustInformation(data){
    return request({
      url:'/spring/punchingCard/adjustInformation',
      method:'put',
      data
    })
  }