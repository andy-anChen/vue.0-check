import request from "@/utils/request";

// 分页模糊查询员工信息

export function getStaff(data) {
  return request({
    url: "/spring/user/page",
    method: "post",
    data,
  });
}

// 查询所有岗位
export function getPost() {
  return request({
    url: "/spring/station/select",
    method: "get",
  });
}

//查询所有供应商
export function getSupplier() {
  return request({
    url: "/spring/provider/select",
    method: "get",
  });
}

// 添加员工信息
export function addStaff(data) {
  return request({
    url: "/spring/user/add",
    method: "post",
    data,
  });
}

// 根据id删除员工信息
export function deletStaff(id) {
  return request({
    url: `/spring/user/delete/${id}`,
    method: "delete",
  });
}

// 根据员工id修改在职离职状态
export function putState(data) {
  return request({
    url: "/spring/user/jobUpdate",
    method: "put",
    params: data,
  });
}

// 根据id修改员工信息

export function editStaff(data) {
  return request({
    url: "/spring/user/update",
    method: "put",
    data,
  });
}
// 导出操作
function formidableUtil(res, fileName) {
  const blob = new Blob([res], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' })
  if ('download' in document.createElement('a')) {
   // 非IE下载
   const elink = document.createElement('a')
   elink.download = fileName
   elink.style.display = 'none'
   elink.href = URL.createObjectURL(blob)
   document.body.appendChild(elink)
   elink.click()
   URL.revokeObjectURL(elink.href) // 释放URL 对象
   document.body.removeChild(elink)
  } else {
   // IE10+下载
   navigator.msSaveBlob(blob, fileName)
  }
}

// 员工导出Excel


//员工excel文件导入
export function userUpload(data) {
  return request({
    url: "/spring/excel/userUpload",
    method: "post",
    data,
  });
}

// 
