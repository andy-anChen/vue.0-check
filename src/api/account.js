import request from '@/utils/request';

// 根据名称和状态分页查询
export function getAccount(data){
    return request({
      url:'/spring/account',
      method:'get',
      params:data
    })
  }

  // 查询所有员工
  export function getUser(){
      return request({
        url:'/spring/user/notAccount',
        method:'get',
      })
    }
  
    // 查询所有角色
    export function getRole(){
        return request({
          url:'/spring/role/select',
          method:'get',
        })
      }

  // 添加账户和中间表
  export function addAccount(data){
      return request({
        url:'/spring/account/add',
        method:'post',
        data
      })
    }

    // 根据账户id删除账户和中间表
    export function deletAccount(userId){
        return request({
          url:`/spring/account/delete/${userId}`,
          method: 'delete'
        })
      }

      // 重置密码
      export function resetPwd(data){
          return request({
            url:'/spring/account/reset',
            method:'put',
            params:data
          })
        }
      //修改角色状态
      export function updateState(data) {
        return request({
          url: '/spring/account/updateState',
          method: 'put',
          params: data
        })
      }

      // 根据id修改账户信息和中间表

      export function update(data){
          return request({
            url:'/spring/account/update',
            method:'put',
            data
          })
        }