import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/spring/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/vue-admin-template/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}

// 修改密码操作
export function updatePwd(data){
    return request({
      url:'/spring/login/updatePwd',
      method:'put',
      params:data
    })
  }
