import request from '@/utils/request';

// 岗位管理分页查询
export function getPost(data) {
    return request({
      url: '/spring/station',
      method: 'get',
      params:data
    })
  }
  
  // 添加岗位信息
  export function addPost(data){
    return request({
      url:'/spring/station/add',
      method:'post',
      data
    })
  }

  // 根据id删除岗位信息
  export function deletePost(id){
    return request({
      url:`/spring/station/delete/${id}`,
      method:'delete'
    })
  }

  // 修改岗位信息
  export function updatePost(data){
    return request({
      url:'/spring/station/update',
      method:'put',
      data
    })
  }
  