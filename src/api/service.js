import request from '@/utils/request'

//分页模糊查询业务线信息
export function getServiceList(data){
    return request({
      url:'/spring/business',
      method:'get',
      params:data
    })
  }

// 添加业务线信息
export function addService(data){
    return request({
      url:'/spring/business/add',
      method:'post',
      data
    })
  }

//   查询角色是业务线管理员的负责人

export function principal(){
    return request({
      url:'/spring/business/principal',
      method:'get',
    })
  }

//   根据id修改业务线信息
export function editService(data){
    return request({
      url:'/spring/business/update',
      method:'put',
      data
    })
  }
  
//   根据id关闭业务线
export function updateCloseState(data) {
    return request({
      url:'/spring/business/updateCloseState',
      method:'put',
      params:data
    })
  }