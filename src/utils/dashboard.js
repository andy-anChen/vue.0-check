/* 
This week we're looking at looops

We've loaded all the GSAP plugins into this pen - so go wild!

Here's a little loop to start you off!
*/
import { gsap } from "gsap";

export function scccc(){
    gsap.fromTo('.r1', {y:-100}, {
        y: 100,
        backgroundColor: "hsla(0, 0%, +=100%)",
        ease: 'sine.inOut',
        duration: 2,
        rotation: 180,
        stagger: {
          amount: 1,
          repeat: -1,
          yoyo: true,
        }
      }).progress(0)
      
      gsap.fromTo('.r2', {y:-100}, {
        y: 100,
        backgroundColor: "hsla(0, 0%, +=100%)",
        ease: 'sine.inOut',
        duration: 2,
        rotation: 180,
        stagger: {
          amount: 1,
          repeat: -1,
          yoyo: true,
        }
      }).progress(0.2)
      
      gsap.fromTo('.r3', {y:-100}, {
        y: 100,
        backgroundColor: "hsla(0, 0%, +=100%)",
        ease: 'sine.inOut',
        duration: 2,
        rotation: 180,
        stagger: {
          amount: 1,
          repeat: -1,
          yoyo: true,
        }
      }).progress(0.4)
      
      gsap.fromTo('.r4', {y:-100}, {
        y: 100,
        backgroundColor: "hsla(0, 0%, +=100%)",
        ease: 'sine.inOut',
        duration: 2,
        rotation: 180,
        stagger: {
          amount: 1,
          repeat: -1,
          yoyo: true,
        }
      }).progress(0.6)
      
      gsap.fromTo('.r5', {y:-100}, {
        y: 100,
        backgroundColor: "hsla(0, 0%, +=100%)",
        ease: 'sine.inOut',
        duration: 2,
        rotation: 180,
        stagger: {
          amount: 1,
          repeat: -1,
          yoyo: true,
        }
      }).progress(0.8)
}